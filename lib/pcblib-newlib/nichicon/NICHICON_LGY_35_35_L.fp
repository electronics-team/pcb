	# grab the input values and convert to 1/100 mil
# element_flags, description, pcb-name, value, mark_x, mark_y,
# text_x, text_y, text_direction, text_scale, text_flags
Element[0x00000000 "Nichicon LGY Series of Radial Snap-in Aluminum Electrolytic Capacitors" "" "NICHICON_LGY_35_35_L" 0 0 -75291 0 0 100 ""]
(
# Pin[x, y, thickness, clearance, mask, drilling hole, name,
#     number, flags
Pin[ -19685 0  9645 1968 9645 7283 "1" "1" "square"]
Pin[  19685 0  9645 1968 9645 7283 "2" "2" 0x0]
# Silk screen around package
ElementArc[0 0 69291 69291 0 360 800]
ElementLine[ -75291 -2500 -70291 -2500 800]
	ElementLine[ -72791 -5000 -72791 0 800]
)
