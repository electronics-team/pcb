	# grab the input values and convert to 1/100 mil
# element_flags, description, pcb-name, value, mark_x, mark_y,
# text_x, text_y, text_direction, text_scale, text_flags
Element[0x00000000 "Nichicon UPT Series of Radial Aluminum Electrolytic Capacitors" "" "NICHICON_UPT_12p5_25_M" 0 0 -32574 0 0 100 ""]
(
# Pin[x, y, thickness, clearance, mask, drilling hole, name,
#     number, flags
Pin[ -9842 0  8031 3956 8031 3346 "1" "1" "square"]
Pin[  9842 0  8031 3956 8031 3346 "2" "2" 0x0]
# Silk screen around package
ElementArc[0 0 26574 26574 0 360 800]
ElementLine[ -32574 -2500 -27574 -2500 800]
	ElementLine[ -30074 -5000 -30074 0 800]
)
