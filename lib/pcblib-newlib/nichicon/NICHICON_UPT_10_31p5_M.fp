	# grab the input values and convert to 1/100 mil
# element_flags, description, pcb-name, value, mark_x, mark_y,
# text_x, text_y, text_direction, text_scale, text_flags
Element[0x00000000 "Nichicon UPT Series of Radial Aluminum Electrolytic Capacitors" "" "NICHICON_UPT_10_31p5_M" 0 0 -27653 0 0 100 ""]
(
# Pin[x, y, thickness, clearance, mask, drilling hole, name,
#     number, flags
Pin[ -9842 0  8031 3956 8031 3346 "1" "1" "square"]
Pin[  9842 0  8031 3956 8031 3346 "2" "2" 0x0]
# Silk screen around package
ElementArc[0 0 21653 21653 0 360 800]
ElementLine[ -27653 -2500 -22653 -2500 800]
	ElementLine[ -25153 -5000 -25153 0 800]
)
