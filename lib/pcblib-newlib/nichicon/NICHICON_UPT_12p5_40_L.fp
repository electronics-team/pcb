	# grab the input values and convert to 1/100 mil
# element_flags, description, pcb-name, value, mark_x, mark_y,
# text_x, text_y, text_direction, text_scale, text_flags
Element[0x00000000 "Nichicon UPT Series of Radial Aluminum Electrolytic Capacitors" "" "NICHICON_UPT_12p5_40_L" 0 0 -31000 0 0 100 ""]
(
# Pin[x, y, thickness, clearance, mask, drilling hole, name,
#     number, flags
Pin[ -9842 0  6062 1969 6062 3740 "1" "1" "square"]
Pin[  9842 0  6062 1969 6062 3740 "2" "2" 0x0]
# Silk screen around package
ElementArc[0 0 25000 25000 0 360 800]
ElementLine[ -31000 -2500 -26000 -2500 800]
	ElementLine[ -28500 -5000 -28500 0 800]
)
