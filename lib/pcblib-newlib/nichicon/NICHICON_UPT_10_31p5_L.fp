	# grab the input values and convert to 1/100 mil
# element_flags, description, pcb-name, value, mark_x, mark_y,
# text_x, text_y, text_direction, text_scale, text_flags
Element[0x00000000 "Nichicon UPT Series of Radial Aluminum Electrolytic Capacitors" "" "NICHICON_UPT_10_31p5_L" 0 0 -26078 0 0 100 ""]
(
# Pin[x, y, thickness, clearance, mask, drilling hole, name,
#     number, flags
Pin[ -9842 0  6062 1969 6062 2952 "1" "1" "square"]
Pin[  9842 0  6062 1969 6062 2952 "2" "2" 0x0]
# Silk screen around package
ElementArc[0 0 20078 20078 0 360 800]
ElementLine[ -26078 -2500 -21078 -2500 800]
	ElementLine[ -23578 -5000 -23578 0 800]
)
