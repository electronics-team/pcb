	# grab the input values and convert to 1/100 mil
# element_flags, description, pcb-name, value, mark_x, mark_y,
# text_x, text_y, text_direction, text_scale, text_flags
Element[0x00000000 "Nichicon LGY Series of Radial Snap-in Aluminum Electrolytic Capacitors" "" "NICHICON_LGY_35_25_N" 0 0 -75881 0 0 100 ""]
(
# Pin[x, y, thickness, clearance, mask, drilling hole, name,
#     number, flags
Pin[ -19685 0  10196 2756 10196 7480 "1" "1" "square"]
Pin[  19685 0  10196 2756 10196 7480 "2" "2" 0x0]
# Silk screen around package
ElementArc[0 0 69881 69881 0 360 800]
ElementLine[ -75881 -2500 -70881 -2500 800]
	ElementLine[ -73381 -5000 -73381 0 800]
)
