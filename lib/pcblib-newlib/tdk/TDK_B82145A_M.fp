	# grab the input values and convert to 1/100 mil
# element_flags, description, pcb-name, value, mark_x, mark_y,
# text_x, text_y, text_direction, text_scale, text_flags
Element[0x00000000 "HLBC Series (B82145A 6.5 x 12 mm) Inductors" "" "TDK_B82145A_M" 0 0 -25590 -14763 0 100 ""]
(
# Pin[x, y, thickness, clearance, mask, drilling hole, name,
#     number, flags 
Pin[ -31496 0  8031 3956 8031 4133 "1" "1" "square"]
Pin[  31496 0  8031 3956 8031 4133 "2" "2" 0x0]
# Silk screen around package
ElementLine[ 25590  14763  25590 -14763 800]
ElementLine[ 25590 -14763 -25590 -14763 800]
ElementLine[-25590 -14763 -25590  14763 800]
ElementLine[-25590  14763  25590  14763 800] 
)
