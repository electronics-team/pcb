	# grab the input values and convert to 1/100 mil
	# add to the pin size to get the drill size and round
	# to the nearest .05mm to get a standard drill.  The
	# +3 causes us to round up
# element_flags, description, pcb-name, value, mark_x, mark_y,
# text_x, text_y, text_direction, text_scale, text_flags
Element[0x00000000 "Toko 7P Variable Coils" "" "TOKO_7P" 0 0 -13779 13779 0 100 ""]
(
# Pin[x, y, thickness, clearance, mask, drilling hole, name,
#     number, flags
# Pad[x1, y1, x2, y2, thickness, clearance, mask, name , pad number, flags]
Pin[  8858  8858 5511 2000 6011 2755 "1" "1" ""]
Pin[      0  8858 5511 2000 6011 2755 "2" "2" ""]
Pin[ -8858  8858 5511 2000 6011 2755 "3" "3" ""]
Pin[ -8858 -8858 5511 2000 6011 2755 "4" "4" ""]
Pin[  8858 -8858 5511 2000 6011 2755 "6" "6" ""]
Pin[  13779  0 7480 2000 7980 4724 "CASE1" "7" 0x0]
Pin[ -13779  0 7480 2000 7980 4724 "CASE2" "8" 0x0]
# Silk screen around package
ElementLine[ 13779  13779  13779 -13779 800]
ElementLine[ 13779 -13779 -13779 -13779 800]
ElementLine[-13779 -13779 -13779  13779 800]
ElementLine[-13779  13779  13779  13779 800] 
)
