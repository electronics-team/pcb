	# machine screw size
Element[ "" "Mounting holes" "" "MTGP1010H370V8L" 0 0 20748 -20748 0 100 ""]
(
# Pin[x, y, thickness, clearance, mask, drilling hole, name,
#     number, flags.  By default, add a solid connection to the plane
Pin[ 0 0  39763  4724 40763 14566 "1" "1" "thermal(0S) "]
ElementArc[0 0 20748 20748 0 360 1000]
Pin[  13582    0 12598  0 0 3937 "1" "1" ""]
Pin[ -13582    0 12598  0 0 3937 "1" "1" ""]
Pin[    0  13582 12598  0 0 3937 "1" "1" ""]
Pin[    0 -13582 12598  0 0 3937 "1" "1" ""]
Pin[  9603  9603 12598  0 0 3937 "1" "1" ""]
Pin[ -9603  9603 12598  0 0 3937 "1" "1" ""]
Pin[  9603 -9603 12598  0 0 3937 "1" "1" ""]
Pin[ -9603 -9603 12598  0 0 3937 "1" "1" ""]
)
