	# machine screw size
Element[ "" "Mounting holes" "" "MTGP970H330V8M" 0 0 21535 -21535 0 100 ""]
(
# Pin[x, y, thickness, clearance, mask, drilling hole, name,
#     number, flags.  By default, add a solid connection to the plane
Pin[ 0 0  38188  7874 39188 12992 "1" "1" "thermal(0S) "]
ElementArc[0 0 21535 21535 0 360 1000]
Pin[  12795    0 12598  0 0 3937 "1" "1" ""]
Pin[ -12795    0 12598  0 0 3937 "1" "1" ""]
Pin[    0  12795 12598  0 0 3937 "1" "1" ""]
Pin[    0 -12795 12598  0 0 3937 "1" "1" ""]
Pin[  9046  9046 12598  0 0 3937 "1" "1" ""]
Pin[ -9046  9046 12598  0 0 3937 "1" "1" ""]
Pin[  9046 -9046 12598  0 0 3937 "1" "1" ""]
Pin[ -9046 -9046 12598  0 0 3937 "1" "1" ""]
)
