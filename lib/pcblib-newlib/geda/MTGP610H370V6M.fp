	# machine screw size
Element[ "" "Mounting holes" "" "MTGP610H370V6M" 0 0 14448 -14448 0 100 ""]
(
# Pin[x, y, thickness, clearance, mask, drilling hole, name,
#     number, flags.  By default, add a solid connection to the plane
Pin[ 0 0  24015  7874 25015 14566 "1" "1" "thermal(0S) "]
ElementArc[0 0 14448 14448 0 360 1000]
Pin[     0   9645  4724  0 0 1968 "1" "1" ""]
Pin[     0  -9645  4724  0 0 1968 "1" "1" ""]
Pin[ -8353   4822 4724  0 0 1968 "1" "1" ""]
Pin[  8353   4822 4724  0 0 1968 "1" "1" ""]
Pin[ -8353  -4822 4724  0 0 1968 "1" "1" ""]
Pin[  8353  -4822 4724  0 0 1968 "1" "1" ""]
)
