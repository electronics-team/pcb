	# machine screw size
Element[ "" "Mounting holes" "" "MTGP730H390V6M" 0 0 16811 -16811 0 100 ""]
(
# Pin[x, y, thickness, clearance, mask, drilling hole, name,
#     number, flags.  By default, add a solid connection to the plane
Pin[ 0 0  28740  7874 29740 15354 "1" "1" "thermal(0S) "]
ElementArc[0 0 16811 16811 0 360 1000]
Pin[     0   11023  6693  0 0 1968 "1" "1" ""]
Pin[     0  -11023  6693  0 0 1968 "1" "1" ""]
Pin[ -9547   5511 6693  0 0 1968 "1" "1" ""]
Pin[  9547   5511 6693  0 0 1968 "1" "1" ""]
Pin[ -9547  -5511 6693  0 0 1968 "1" "1" ""]
Pin[  9547  -5511 6693  0 0 1968 "1" "1" ""]
)
