	# machine screw size
Element[ "" "Mounting holes" "" "MTGP1200H390V8L" 0 0 24488 -24488 0 100 ""]
(
# Pin[x, y, thickness, clearance, mask, drilling hole, name,
#     number, flags.  By default, add a solid connection to the plane
Pin[ 0 0  47244  4724 48244 15354 "1" "1" "thermal(0S) "]
ElementArc[0 0 24488 24488 0 360 1000]
Pin[     0   15649  15945  0 0 3937 "1" "1" ""]
Pin[     0  -15649  15945  0 0 3937 "1" "1" ""]
Pin[ -13553   7824 15945  0 0 3937 "1" "1" ""]
Pin[  13553   7824 15945  0 0 3937 "1" "1" ""]
Pin[ -13553  -7824 15945  0 0 3937 "1" "1" ""]
Pin[  13553  -7824 15945  0 0 3937 "1" "1" ""]
)
