	# machine screw size
Element[ "" "Mounting holes" "" "MTGP1010H370V8N" 0 0 21338 -21338 0 100 ""]
(
# Pin[x, y, thickness, clearance, mask, drilling hole, name,
#     number, flags.  By default, add a solid connection to the plane
Pin[ 0 0  39763  5905 40763 14566 "1" "1" "thermal(0S) "]
ElementArc[0 0 21338 21338 0 360 1000]
Pin[  13582    0 12598  0 0 3937 "1" "1" ""]
Pin[ -13582    0 12598  0 0 3937 "1" "1" ""]
Pin[    0  13582 12598  0 0 3937 "1" "1" ""]
Pin[    0 -13582 12598  0 0 3937 "1" "1" ""]
Pin[  9603  9603 12598  0 0 3937 "1" "1" ""]
Pin[ -9603  9603 12598  0 0 3937 "1" "1" ""]
Pin[  9603 -9603 12598  0 0 3937 "1" "1" ""]
Pin[ -9603 -9603 12598  0 0 3937 "1" "1" ""]
)
