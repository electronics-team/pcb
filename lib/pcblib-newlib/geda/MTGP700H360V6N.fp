	# machine screw size
Element[ "" "Mounting holes" "" "MTGP700H360V6N" 0 0 15236 -15236 0 100 ""]
(
# Pin[x, y, thickness, clearance, mask, drilling hole, name,
#     number, flags.  By default, add a solid connection to the plane
Pin[ 0 0  27559  5905 28559 14173 "1" "1" "thermal(0S) "]
ElementArc[0 0 15236 15236 0 360 1000]
Pin[     0   10433  6693  0 0 1968 "1" "1" ""]
Pin[     0  -10433  6693  0 0 1968 "1" "1" ""]
Pin[ -9035   5216 6693  0 0 1968 "1" "1" ""]
Pin[  9035   5216 6693  0 0 1968 "1" "1" ""]
Pin[ -9035  -5216 6693  0 0 1968 "1" "1" ""]
Pin[  9035  -5216 6693  0 0 1968 "1" "1" ""]
)
