	# machine screw size
Element[ "" "Mounting holes" "" "MTGP570H330V6N" 0 0 12677 -12677 0 100 ""]
(
# Pin[x, y, thickness, clearance, mask, drilling hole, name,
#     number, flags.  By default, add a solid connection to the plane
Pin[ 0 0  22440  5905 23440 12992 "1" "1" "thermal(0S) "]
ElementArc[0 0 12677 12677 0 360 1000]
Pin[     0   8858  4724  0 0 1968 "1" "1" ""]
Pin[     0  -8858  4724  0 0 1968 "1" "1" ""]
Pin[ -7671   4429 4724  0 0 1968 "1" "1" ""]
Pin[  7671   4429 4724  0 0 1968 "1" "1" ""]
Pin[ -7671  -4429 4724  0 0 1968 "1" "1" ""]
Pin[  7671  -4429 4724  0 0 1968 "1" "1" ""]
)
