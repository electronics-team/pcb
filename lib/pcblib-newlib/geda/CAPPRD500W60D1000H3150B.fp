	# grab the input values and convert to 1/100 mil
# element_flags, description, pcb-name, value, mark_x, mark_y,
# text_x, text_y, text_direction, text_scale, text_flags
Element[0x00000000 "Capacitors, radial IPC-7251 compliant" "" "CAPPRD500W60D1000H3150B" 0 0 -26669 0 0 100 ""]
(
# Pin[x, y, thickness, clearance, mask, drilling hole, name,
#     number, flags
Pin[ -9842 0  6692 2736 6692 3149 "1" "1" "square"]
Pin[  9842 0  6692 2736 6692 3149 "2" "2" 0x0]
# Silk screen around package
ElementArc[0 0 20669 20669 0 360 800]
ElementLine[ -26669 -2500 -21669 -2500 800]
	ElementLine[ -24169 -5000 -24169 0 800]
)
