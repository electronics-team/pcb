	# machine screw size
Element[ "" "Mounting holes" "" "MTGP1170H360V8M" 0 0 25472 -25472 0 100 ""]
(
# Pin[x, y, thickness, clearance, mask, drilling hole, name,
#     number, flags.  By default, add a solid connection to the plane
Pin[ 0 0  46062  7874 47062 14173 "1" "1" "thermal(0S) "]
ElementArc[0 0 25472 25472 0 360 1000]
Pin[     0   15058  15944  0 0 3937 "1" "1" ""]
Pin[     0  -15058  15944  0 0 3937 "1" "1" ""]
Pin[ -13041   7529 15944  0 0 3937 "1" "1" ""]
Pin[  13041   7529 15944  0 0 3937 "1" "1" ""]
Pin[ -13041  -7529 15944  0 0 3937 "1" "1" ""]
Pin[  13041  -7529 15944  0 0 3937 "1" "1" ""]
)
