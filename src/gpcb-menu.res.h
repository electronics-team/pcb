# 56 "gpcb-menu.res"
char *s = N_("File");
# 57 "gpcb-menu.res"
char *s = N_("New");
# 57 "gpcb-menu.res"
char *s = N_("Ctrl-N");
# 57 "gpcb-menu.res"
char *s = N_("Ctrl<Key>n");
# 58 "gpcb-menu.res"
char *s = N_("Open...");
# 58 "gpcb-menu.res"
char *s = N_("Load a layout from a file");
# 60 "gpcb-menu.res"
char *s = N_("Save");
# 60 "gpcb-menu.res"
char *s = N_("Saves current layout");
# 60 "gpcb-menu.res"
char *s = N_("Ctrl-S");
# 60 "gpcb-menu.res"
char *s = N_("Ctrl<Key>s");
# 61 "gpcb-menu.res"
char *s = N_("Save As...");
# 61 "gpcb-menu.res"
char *s = N_("Saves current layout into a new file");
# 61 "gpcb-menu.res"
char *s = N_("Shift Ctrl-S");
# 61 "gpcb-menu.res"
char *s = N_("Shift Ctrl<Key>s");
# 62 "gpcb-menu.res"
char *s = N_("Revert");
# 62 "gpcb-menu.res"
char *s = N_("Revert to the layout stored on disk");
# 64 "gpcb-menu.res"
char *s = N_("Import Schematics");
# 65 "gpcb-menu.res"
char *s = N_("gschem");
# 66 "gpcb-menu.res"
char *s = N_("TinyCAD");
# 68 "gpcb-menu.res"
char *s = N_("Load element to buffer");
# 69 "gpcb-menu.res"
char *s = N_("Load layout to buffer");
# 70 "gpcb-menu.res"
char *s = N_("Load netlist");
# 71 "gpcb-menu.res"
char *s = N_("Load vendor resource file");
# 73 "gpcb-menu.res"
char *s = N_("Save connection data of");
# 74 "gpcb-menu.res"
char *s = N_(" a single element");
# 75 "gpcb-menu.res"
char *s = N_(" all elements");
# 76 "gpcb-menu.res"
char *s = N_(" unused pins");
# 78 "gpcb-menu.res"
char *s = N_("Export...");
# 80 "gpcb-menu.res"
char *s = N_("Calibrate Printer...");
# 81 "gpcb-menu.res"
char *s = N_("Print...");
# 83 "gpcb-menu.res"
char *s = N_("Preferences...");
# 85 "gpcb-menu.res"
char *s = N_("Quit");
# 85 "gpcb-menu.res"
char *s = N_("Ctrl-Q");
# 85 "gpcb-menu.res"
char *s = N_("Ctrl<Key>q");
# 91 "gpcb-menu.res"
char *s = N_("Edit");
# 92 "gpcb-menu.res"
char *s = N_("Undo");
# 92 "gpcb-menu.res"
char *s = N_("U");
# 92 "gpcb-menu.res"
char *s = N_("<Key>u");
# 93 "gpcb-menu.res"
char *s = N_("Redo");
# 93 "gpcb-menu.res"
char *s = N_("Shift-R");
# 93 "gpcb-menu.res"
char *s = N_("Shift<Key>r");
# 94 "gpcb-menu.res"
char *s = N_("Clear undo-buffer");
# 94 "gpcb-menu.res"
char *s = N_("Shift-Ctrl-U");
# 94 "gpcb-menu.res"
char *s = N_("Shift Ctrl<Key>u");
# 313 "gpcb-menu.res"
char *s = N_("Cut to buffer");
# 98 "gpcb-menu.res"
char *s = N_("Ctrl-X");
# 98 "gpcb-menu.res"
char *s = N_("Ctrl<Key>x");
# 99 "gpcb-menu.res"
char *s = N_("Copy to buffer");
# 101 "gpcb-menu.res"
char *s = N_("Ctrl-C");
# 101 "gpcb-menu.res"
char *s = N_("Ctrl<Key>c");
# 315 "gpcb-menu.res"
char *s = N_("Paste buffer");
# 102 "gpcb-menu.res"
char *s = N_("Ctrl-V");
# 102 "gpcb-menu.res"
char *s = N_("Ctrl<Key>v");
# 251 "gpcb-menu.res"
char *s = N_("Unselect all");
# 104 "gpcb-menu.res"
char *s = N_("Shift-Ctrl-A");
# 104 "gpcb-menu.res"
char *s = N_("Shift Ctrl<Key>a");
# 246 "gpcb-menu.res"
char *s = N_("Select all visible");
# 105 "gpcb-menu.res"
char *s = N_("Ctrl-A");
# 105 "gpcb-menu.res"
char *s = N_("Ctrl<Key>a");
# 107 "gpcb-menu.res"
char *s = N_("Edit name of");
# 108 "gpcb-menu.res"
char *s = N_("text on layout");
# 108 "gpcb-menu.res"
char *s = N_("N");
# 108 "gpcb-menu.res"
char *s = N_("<Key>n");
# 109 "gpcb-menu.res"
char *s = N_("layout");
# 110 "gpcb-menu.res"
char *s = N_("active layer");
# 112 "gpcb-menu.res"
char *s = N_("Edit attributes of");
# 113 "gpcb-menu.res"
char *s = N_("Layout");
# 114 "gpcb-menu.res"
char *s = N_("CurrentLayer");
# 115 "gpcb-menu.res"
char *s = N_("Element");
# 118 "gpcb-menu.res"
char *s = N_("Route Styles");
# 120 "gpcb-menu.res"
char *s = N_("Edit...");
# 123 "gpcb-menu.res"
char *s = N_("Via type");
# 303 "gpcb-menu.res"
char *s = N_("Through-hole");
# 124 "gpcb-menu.res"
char *s = N_("Xtrl-Shift-P");
# 228 "gpcb-menu.res"
char *s = N_("Ctrl Shift<Key>p");
# 125 "gpcb-menu.res"
char *s = N_("Buried from");
# 125 "gpcb-menu.res"
char *s = N_("Xtrl-Shift-F");
# 125 "gpcb-menu.res"
char *s = N_("Ctrl Shift<Key>f");
# 126 "gpcb-menu.res"
char *s = N_("Buried to");
# 126 "gpcb-menu.res"
char *s = N_("Xtrl-Shift-T");
# 126 "gpcb-menu.res"
char *s = N_("Ctrl Shift<Key>t");
# 133 "gpcb-menu.res"
char *s = N_("View");
# 134 "gpcb-menu.res"
char *s = N_("Enable visible grid");
# 135 "gpcb-menu.res"
char *s = N_("Grid units");
# 136 "gpcb-menu.res"
char *s = N_("mil");
# 137 "gpcb-menu.res"
char *s = N_("mm");
# 139 "gpcb-menu.res"
char *s = N_("Grid size");
# 140 "gpcb-menu.res"
char *s = N_("No Grid");
# 142 "gpcb-menu.res"
char *s = N_("0.1 mil");
# 143 "gpcb-menu.res"
char *s = N_("1 mil");
# 144 "gpcb-menu.res"
char *s = N_("5 mil");
# 145 "gpcb-menu.res"
char *s = N_("10 mil");
# 146 "gpcb-menu.res"
char *s = N_("25 mil");
# 147 "gpcb-menu.res"
char *s = N_("50 mil");
# 148 "gpcb-menu.res"
char *s = N_("100 mil");
# 150 "gpcb-menu.res"
char *s = N_("0.01 mm");
# 151 "gpcb-menu.res"
char *s = N_("0.05 mm");
# 152 "gpcb-menu.res"
char *s = N_("0.1 mm");
# 153 "gpcb-menu.res"
char *s = N_("0.25 mm");
# 154 "gpcb-menu.res"
char *s = N_("0.5 mm");
# 155 "gpcb-menu.res"
char *s = N_("1 mm");
# 157 "gpcb-menu.res"
char *s = N_("Grid -");
# 157 "gpcb-menu.res"
char *s = N_("Shift-G");
# 157 "gpcb-menu.res"
char *s = N_("Shift<Key>g");
# 158 "gpcb-menu.res"
char *s = N_("Grid +");
# 158 "gpcb-menu.res"
char *s = N_("G");
# 158 "gpcb-menu.res"
char *s = N_("<Key>g");
# 160 "gpcb-menu.res"
char *s = N_("Realign grid");
# 162 "gpcb-menu.res"
char *s = N_("Displayed element name");
# 163 "gpcb-menu.res"
char *s = N_("Description");
# 164 "gpcb-menu.res"
char *s = N_("Reference Designator");
# 165 "gpcb-menu.res"
char *s = N_("Value");
# 167 "gpcb-menu.res"
char *s = N_("Enable Pinout shows number");
# 168 "gpcb-menu.res"
char *s = N_("Pins/Via show Name/Number");
# 168 "gpcb-menu.res"
char *s = N_("D");
# 168 "gpcb-menu.res"
char *s = N_("<Key>d");
# 169 "gpcb-menu.res"
/* xgettext:no-c-format */
char *s = N_("Zoom In 20%");
# 170 "gpcb-menu.res"
char *s = N_("Z");
# 170 "gpcb-menu.res"
char *s = N_("<Key>z");
# 170 "gpcb-menu.res"
/* xgettext:no-c-format */
char *s = N_("Zoom Out 20%");
# 171 "gpcb-menu.res"
char *s = N_("Shift-Z");
# 171 "gpcb-menu.res"
char *s = N_("Shift<Key>z");
# 172 "gpcb-menu.res"
char *s = N_("More zooms and view changes");
# 173 "gpcb-menu.res"
char *s = N_("Zoom Max");
# 173 "gpcb-menu.res"
char *s = N_("V");
# 173 "gpcb-menu.res"
char *s = N_("<Key>v");
# 174 "gpcb-menu.res"
char *s = N_("Zoom In 2X");
# 175 "gpcb-menu.res"
char *s = N_("Zoom Out 2X");
# 176 "gpcb-menu.res"
char *s = N_("Zoom to 0.1mil/px");
# 177 "gpcb-menu.res"
char *s = N_("Zoom to 0.01mm/px");
# 178 "gpcb-menu.res"
char *s = N_("Zoom to 1mil/px");
# 179 "gpcb-menu.res"
char *s = N_("Zoom to 0.05mm/px");
# 180 "gpcb-menu.res"
char *s = N_("Zoom to 2.5mil/px");
# 181 "gpcb-menu.res"
char *s = N_("Zoom to 0.1mm/px");
# 182 "gpcb-menu.res"
char *s = N_("Zoom to 10mil/px");
# 182 "gpcb-menu.res"
/* xgettext:no-c-format */
char *s = N_("Zoom In 20% and center");
# 183 "gpcb-menu.res"
/* xgettext:no-c-format */
char *s = N_("Zoom Out 20% and center");
# 185 "gpcb-menu.res"
char *s = N_("Flip up/down");
# 185 "gpcb-menu.res"
char *s = N_("Tab");
# 185 "gpcb-menu.res"
char *s = N_("<Key>Tab");
# 186 "gpcb-menu.res"
char *s = N_("Flip left/right");
# 186 "gpcb-menu.res"
char *s = N_("Shift-Tab");
# 186 "gpcb-menu.res"
char *s = N_("Shift<Key>Tab");
# 187 "gpcb-menu.res"
char *s = N_("Spin 180 degrees");
# 187 "gpcb-menu.res"
char *s = N_("Ctrl-Tab");
# 187 "gpcb-menu.res"
char *s = N_("Ctrl<Key>Tab");
# 188 "gpcb-menu.res"
char *s = N_("Swap Sides");
# 188 "gpcb-menu.res"
char *s = N_("Ctrl-Shift-Tab");
# 188 "gpcb-menu.res"
char *s = N_("Ctrl Shift<Key>Tab");
# 189 "gpcb-menu.res"
char *s = N_("Center cursor");
# 189 "gpcb-menu.res"
char *s = N_("C");
# 189 "gpcb-menu.res"
char *s = N_("<Key>c");
# 192 "gpcb-menu.res"
char *s = N_("Shown Layers");
# 195 "gpcb-menu.res"
char *s = N_("Edit Layer Groups");
# 197 "gpcb-menu.res"
char *s = N_("Current Layer");
# 200 "gpcb-menu.res"
char *s = N_("Delete current layer");
# 201 "gpcb-menu.res"
char *s = N_("Add new layer");
# 202 "gpcb-menu.res"
char *s = N_("Move current layer up");
# 203 "gpcb-menu.res"
char *s = N_("Move current layer down");
# 210 "gpcb-menu.res"
char *s = N_("Settings");
# 211 "gpcb-menu.res"
char *s = N_("'All-direction' lines");
# 211 "gpcb-menu.res"
char *s = N_(".");
# 211 "gpcb-menu.res"
char *s = N_("<Key>.");
# 212 "gpcb-menu.res"
char *s = N_("Auto swap line start angle");
# 213 "gpcb-menu.res"
char *s = N_("Orthogonal moves");
# 214 "gpcb-menu.res"
char *s = N_("Crosshair snaps to pins and pads");
# 215 "gpcb-menu.res"
char *s = N_("Crosshair shows DRC clearance");
# 216 "gpcb-menu.res"
char *s = N_("Auto enforce DRC clearance");
# 217 "gpcb-menu.res"
char *s = N_("Lock Names");
# 218 "gpcb-menu.res"
char *s = N_("Only Names");
# 219 "gpcb-menu.res"
char *s = N_("Hide Names");
# 221 "gpcb-menu.res"
char *s = N_("Rubber band mode");
# 222 "gpcb-menu.res"
char *s = N_("Require unique element names");
# 223 "gpcb-menu.res"
char *s = N_("Auto-zero delta measurements");
# 224 "gpcb-menu.res"
char *s = N_("New lines, arcs clear polygons");
# 225 "gpcb-menu.res"
char *s = N_("New polygons are full ones");
# 226 "gpcb-menu.res"
char *s = N_("Show autorouter trials");
# 227 "gpcb-menu.res"
char *s = N_("Thin draw");
# 227 "gpcb-menu.res"
char *s = N_("|");
# 227 "gpcb-menu.res"
char *s = N_("<Key>|");
# 228 "gpcb-menu.res"
char *s = N_("Thin draw poly");
# 228 "gpcb-menu.res"
char *s = N_("Ctrl-Shift-P");
# 229 "gpcb-menu.res"
char *s = N_("Check polygons");
# 230 "gpcb-menu.res"
char *s = N_("Auto buried vias");
# 232 "gpcb-menu.res"
char *s = N_("Vendor drill mapping");
# 233 "gpcb-menu.res"
char *s = N_("Import New Elements at");
# 234 "gpcb-menu.res"
char *s = N_(" Center");
# 235 "gpcb-menu.res"
char *s = N_(" Mark");
# 236 "gpcb-menu.res"
char *s = N_(" Crosshair");
# 238 "gpcb-menu.res"
char *s = N_("Set Dispersion");
# 245 "gpcb-menu.res"
char *s = N_("Select");
# 247 "gpcb-menu.res"
char *s = N_("Select all found");
# 248 "gpcb-menu.res"
char *s = N_("Select all connected");
# 249 "gpcb-menu.res"
char *s = N_("Select all buried vias");
# 252 "gpcb-menu.res"
char *s = N_("Unselect all found");
# 253 "gpcb-menu.res"
char *s = N_("Unselect all connected");
# 255 "gpcb-menu.res"
char *s = N_("Select by name");
# 256 "gpcb-menu.res"
char *s = N_("All objects");
# 298 "gpcb-menu.res"
char *s = N_("Elements");
# 258 "gpcb-menu.res"
char *s = N_("Pads");
# 299 "gpcb-menu.res"
char *s = N_("Pins");
# 524 "gpcb-menu.res"
char *s = N_("Text");
# 261 "gpcb-menu.res"
char *s = N_("Vias");
# 264 "gpcb-menu.res"
char *s = N_("Auto-place selected elements");
# 264 "gpcb-menu.res"
char *s = N_("Ctrl-P");
# 264 "gpcb-menu.res"
char *s = N_("Ctrl<Key>p");
# 265 "gpcb-menu.res"
char *s = N_("Disperse all elements");
# 266 "gpcb-menu.res"
char *s = N_("Disperse selected elements");
# 268 "gpcb-menu.res"
char *s = N_("Move selected elements to other side");
# 268 "gpcb-menu.res"
char *s = N_("Shift-B");
# 268 "gpcb-menu.res"
char *s = N_("Shift<Key>b");
# 269 "gpcb-menu.res"
char *s = N_("Move selected to current layer");
# 269 "gpcb-menu.res"
char *s = N_("Shift-M");
# 269 "gpcb-menu.res"
char *s = N_("Shift<Key>m");
# 493 "gpcb-menu.res"
char *s = N_("Remove selected objects");
# 270 "gpcb-menu.res"
char *s = N_("Shift-Delete");
# 270 "gpcb-menu.res"
char *s = N_("Shift<Key>Delete");
# 507 "gpcb-menu.res"
char *s = N_("Convert selection to element");
# 273 "gpcb-menu.res"
char *s = N_("Optimize selected rats");
# 351 "gpcb-menu.res"
char *s = N_("Auto-route selected rats");
# 274 "gpcb-menu.res"
char *s = N_("Alt-R");
# 274 "gpcb-menu.res"
char *s = N_("Alt<Key>r");
# 510 "gpcb-menu.res"
char *s = N_("Rip up selected auto-routed tracks");
# 277 "gpcb-menu.res"
char *s = N_("Change size of selected objects");
# 278 "gpcb-menu.res"
char *s = N_("Lines -10 mil");
# 279 "gpcb-menu.res"
char *s = N_("Lines +10 mil");
# 280 "gpcb-menu.res"
char *s = N_("Pads -10 mil");
# 281 "gpcb-menu.res"
char *s = N_("Pads +10 mil");
# 293 "gpcb-menu.res"
char *s = N_("Pins -10 mil");
# 294 "gpcb-menu.res"
char *s = N_("Pins +10 mil");
# 284 "gpcb-menu.res"
char *s = N_("Texts -10 mil");
# 285 "gpcb-menu.res"
char *s = N_("Texts +10 mil");
# 291 "gpcb-menu.res"
char *s = N_("Vias -10 mil");
# 292 "gpcb-menu.res"
char *s = N_("Vias +10 mil");
# 290 "gpcb-menu.res"
char *s = N_("Change drilling hole of selected objects");
# 297 "gpcb-menu.res"
char *s = N_("Change square-flag of selected objects");
# 302 "gpcb-menu.res"
char *s = N_("Change type of selected vias");
# 304 "gpcb-menu.res"
char *s = N_("Buried from current layer");
# 305 "gpcb-menu.res"
char *s = N_("Buried to current layer");
# 528 "gpcb-menu.res"
char *s = N_("Buffer");
# 317 "gpcb-menu.res"
char *s = N_("Rotate buffer 90 deg CCW");
# 318 "gpcb-menu.res"
char *s = N_("Shift-F7");
# 318 "gpcb-menu.res"
char *s = N_("Shift<Key>F7");
# 319 "gpcb-menu.res"
char *s = N_("Rotate buffer 90 deg CW");
# 320 "gpcb-menu.res"
char *s = N_("Arbitrarily Rotate Buffer");
# 321 "gpcb-menu.res"
char *s = N_("Mirror buffer (up/down)");
# 322 "gpcb-menu.res"
char *s = N_("Mirror buffer (left/right)");
# 325 "gpcb-menu.res"
char *s = N_("Clear buffer");
# 326 "gpcb-menu.res"
char *s = N_("Convert buffer to element");
# 327 "gpcb-menu.res"
char *s = N_("Break buffer elements to pieces");
# 328 "gpcb-menu.res"
char *s = N_("Save buffer elements to file");
# 330 "gpcb-menu.res"
char *s = N_("Select Buffer #1");
# 330 "gpcb-menu.res"
char *s = N_("Shift-1");
# 330 "gpcb-menu.res"
char *s = N_("Shift<Key>1");
# 331 "gpcb-menu.res"
char *s = N_("Select Buffer #2");
# 331 "gpcb-menu.res"
char *s = N_("Shift-2");
# 331 "gpcb-menu.res"
char *s = N_("Shift<Key>2");
# 332 "gpcb-menu.res"
char *s = N_("Select Buffer #3");
# 332 "gpcb-menu.res"
char *s = N_("Shift-3");
# 332 "gpcb-menu.res"
char *s = N_("Shift<Key>3");
# 333 "gpcb-menu.res"
char *s = N_("Select Buffer #4");
# 333 "gpcb-menu.res"
char *s = N_("Shift-4");
# 333 "gpcb-menu.res"
char *s = N_("Shift<Key>4");
# 334 "gpcb-menu.res"
char *s = N_("Select Buffer #5");
# 334 "gpcb-menu.res"
char *s = N_("Shift-5");
# 334 "gpcb-menu.res"
char *s = N_("Shift<Key>5");
# 340 "gpcb-menu.res"
char *s = N_("Connects");
# 341 "gpcb-menu.res"
char *s = N_("Lookup connection");
# 341 "gpcb-menu.res"
char *s = N_("Ctrl-F");
# 341 "gpcb-menu.res"
char *s = N_("Ctrl<Key>f");
# 342 "gpcb-menu.res"
char *s = N_("Reset scanned pads/pins/vias");
# 343 "gpcb-menu.res"
char *s = N_("Reset scanned lines/polygons");
# 344 "gpcb-menu.res"
char *s = N_("Reset all connections");
# 344 "gpcb-menu.res"
char *s = N_("Shift-F");
# 344 "gpcb-menu.res"
char *s = N_("Shift<Key>f");
# 346 "gpcb-menu.res"
char *s = N_("Optimize rats nest");
# 347 "gpcb-menu.res"
char *s = N_("O");
# 347 "gpcb-menu.res"
char *s = N_("<Key>o");
# 348 "gpcb-menu.res"
char *s = N_("Erase rats nest");
# 348 "gpcb-menu.res"
char *s = N_("E");
# 348 "gpcb-menu.res"
char *s = N_("<Key>e");
# 349 "gpcb-menu.res"
char *s = N_("Erase selected rats");
# 349 "gpcb-menu.res"
char *s = N_("Shift-E");
# 349 "gpcb-menu.res"
char *s = N_("Shift<Key>e");
# 352 "gpcb-menu.res"
char *s = N_("Auto-route all rats");
# 353 "gpcb-menu.res"
char *s = N_("Toporouter");
# 354 "gpcb-menu.res"
char *s = N_("Rip up all auto-routed tracks");
# 356 "gpcb-menu.res"
char *s = N_("Optimize routed tracks");
# 357 "gpcb-menu.res"
char *s = N_("Auto-Optimize");
# 357 "gpcb-menu.res"
char *s = N_("Shift-=");
# 357 "gpcb-menu.res"
char *s = N_("Shift<Key>=");
# 358 "gpcb-menu.res"
char *s = N_("Debumpify");
# 359 "gpcb-menu.res"
char *s = N_("Unjaggy");
# 360 "gpcb-menu.res"
char *s = N_("Vianudge");
# 361 "gpcb-menu.res"
char *s = N_("Viatrim");
# 362 "gpcb-menu.res"
char *s = N_("Ortho pull");
# 363 "gpcb-menu.res"
char *s = N_("Simple optimization");
# 363 "gpcb-menu.res"
char *s = N_("=");
# 363 "gpcb-menu.res"
char *s = N_("<Key>=");
# 364 "gpcb-menu.res"
char *s = N_("Miter");
# 365 "gpcb-menu.res"
char *s = N_("Puller");
# 365 "gpcb-menu.res"
char *s = N_("Y");
# 365 "gpcb-menu.res"
char *s = N_("<Key>y");
# 366 "gpcb-menu.res"
char *s = N_("Global Puller");
# 367 "gpcb-menu.res"
char *s = N_("Selected");
# 368 "gpcb-menu.res"
char *s = N_("Found");
# 369 "gpcb-menu.res"
char *s = N_("All");
# 372 "gpcb-menu.res"
char *s = N_("Only autorouted nets");
# 375 "gpcb-menu.res"
char *s = N_("Design Rule Checker");
# 377 "gpcb-menu.res"
char *s = N_("Apply vendor drill mapping");
# 383 "gpcb-menu.res"
char *s = N_("Info");
# 513 "gpcb-menu.res"
char *s = N_("Generate object report");
# 384 "gpcb-menu.res"
char *s = N_("Ctrl-R");
# 384 "gpcb-menu.res"
char *s = N_("Ctrl<Key>r");
# 385 "gpcb-menu.res"
char *s = N_("Generate drill summary");
# 386 "gpcb-menu.res"
char *s = N_("Report found pins/pads");
# 387 "gpcb-menu.res"
char *s = N_("Report net length");
# 387 "gpcb-menu.res"
char *s = N_("R");
# 387 "gpcb-menu.res"
char *s = N_("<Key>r");
# 388 "gpcb-menu.res"
char *s = N_("Key Bindings");
# 529 "gpcb-menu.res"
char *s = N_("Remove");
# 389 "gpcb-menu.res"
char *s = N_("Delete");
# 389 "gpcb-menu.res"
char *s = N_("<Key>Delete");
# 395 "gpcb-menu.res"
char *s = N_("Remove Selected");
# 395 "gpcb-menu.res"
char *s = N_("Backspace");
# 395 "gpcb-menu.res"
char *s = N_("<Key>BackSpace");
# 398 "gpcb-menu.res"
char *s = N_("Remove Connected");
# 398 "gpcb-menu.res"
char *s = N_("Shift-Backspace");
# 398 "gpcb-menu.res"
char *s = N_("Shift<Key>BackSpace");
# 415 "gpcb-menu.res"
char *s = N_("Set Same");
# 415 "gpcb-menu.res"
char *s = N_("A");
# 415 "gpcb-menu.res"
char *s = N_("<Key>a");
# 416 "gpcb-menu.res"
char *s = N_("Flip Object");
# 416 "gpcb-menu.res"
char *s = N_("B");
# 416 "gpcb-menu.res"
char *s = N_("<Key>b");
# 417 "gpcb-menu.res"
char *s = N_("Find Connections");
# 417 "gpcb-menu.res"
char *s = N_("F");
# 417 "gpcb-menu.res"
char *s = N_("<Key>f");
# 418 "gpcb-menu.res"
char *s = N_("ToggleHideName Object");
# 418 "gpcb-menu.res"
char *s = N_("H");
# 418 "gpcb-menu.res"
char *s = N_("<Key>h");
# 419 "gpcb-menu.res"
char *s = N_("ToggleHideName SelectedElement");
# 419 "gpcb-menu.res"
char *s = N_("Shift-H");
# 419 "gpcb-menu.res"
char *s = N_("Shift<Key>h");
# 420 "gpcb-menu.res"
char *s = N_("ChangeHole Object");
# 420 "gpcb-menu.res"
char *s = N_("Ctrl-H");
# 420 "gpcb-menu.res"
char *s = N_("Ctrl<Key>h");
# 421 "gpcb-menu.res"
char *s = N_("ChangeJoin Object");
# 421 "gpcb-menu.res"
char *s = N_("J");
# 421 "gpcb-menu.res"
char *s = N_("<Key>j");
# 422 "gpcb-menu.res"
char *s = N_("ChangeJoin SelectedObject");
# 422 "gpcb-menu.res"
char *s = N_("Shift-J");
# 422 "gpcb-menu.res"
char *s = N_("Shift<Key>j");
# 423 "gpcb-menu.res"
char *s = N_("Clear Object +");
# 423 "gpcb-menu.res"
char *s = N_("K");
# 423 "gpcb-menu.res"
char *s = N_("<Key>k");
# 424 "gpcb-menu.res"
char *s = N_("Clear Object -");
# 424 "gpcb-menu.res"
char *s = N_("Shift-K");
# 424 "gpcb-menu.res"
char *s = N_("Shift<Key>k");
# 425 "gpcb-menu.res"
char *s = N_("Clear Selected +");
# 425 "gpcb-menu.res"
char *s = N_("Ctrl-K");
# 425 "gpcb-menu.res"
char *s = N_("Ctrl<Key>k");
# 426 "gpcb-menu.res"
char *s = N_("Clear Selected -");
# 426 "gpcb-menu.res"
char *s = N_("Shift-Ctrl-K");
# 426 "gpcb-menu.res"
char *s = N_("Shift Ctrl<Key>k");
# 427 "gpcb-menu.res"
char *s = N_("Line Tool size +");
# 427 "gpcb-menu.res"
char *s = N_("L");
# 427 "gpcb-menu.res"
char *s = N_("<Key>l");
# 428 "gpcb-menu.res"
char *s = N_("Line Tool size -");
# 428 "gpcb-menu.res"
char *s = N_("Shift-L");
# 428 "gpcb-menu.res"
char *s = N_("Shift<Key>l");
# 429 "gpcb-menu.res"
char *s = N_("Move Object to current layer");
# 429 "gpcb-menu.res"
char *s = N_("M");
# 429 "gpcb-menu.res"
char *s = N_("<Key>m");
# 430 "gpcb-menu.res"
char *s = N_("MarkCrosshair");
# 430 "gpcb-menu.res"
char *s = N_("Ctrl-M");
# 430 "gpcb-menu.res"
char *s = N_("Ctrl<Key>m");
# 431 "gpcb-menu.res"
char *s = N_("Select shortest rat");
# 431 "gpcb-menu.res"
char *s = N_("Shift-N");
# 431 "gpcb-menu.res"
char *s = N_("Shift<Key>n");
# 432 "gpcb-menu.res"
char *s = N_("AddRats to selected pins");
# 432 "gpcb-menu.res"
char *s = N_("Shift-O");
# 432 "gpcb-menu.res"
char *s = N_("Shift<Key>o");
# 438 "gpcb-menu.res"
char *s = N_("ChangeOctagon Object");
# 438 "gpcb-menu.res"
char *s = N_("Ctrl-O");
# 438 "gpcb-menu.res"
char *s = N_("Ctrl<Key>o");
# 439 "gpcb-menu.res"
char *s = N_("Polygon PreviousPoint");
# 439 "gpcb-menu.res"
char *s = N_("P");
# 439 "gpcb-menu.res"
char *s = N_("<Key>p");
# 440 "gpcb-menu.res"
char *s = N_("Polygon Close");
# 440 "gpcb-menu.res"
char *s = N_("Shift-P");
# 440 "gpcb-menu.res"
char *s = N_("Shift<Key>p");
# 441 "gpcb-menu.res"
char *s = N_("ChangeSquare Object");
# 441 "gpcb-menu.res"
char *s = N_("Q");
# 441 "gpcb-menu.res"
char *s = N_("<Key>q");
# 442 "gpcb-menu.res"
char *s = N_("ChangeSize +");
# 442 "gpcb-menu.res"
char *s = N_("S");
# 442 "gpcb-menu.res"
char *s = N_("<Key>s");
# 443 "gpcb-menu.res"
char *s = N_("ChangeSize -");
# 443 "gpcb-menu.res"
char *s = N_("Shift-S");
# 443 "gpcb-menu.res"
char *s = N_("Shift<Key>s");
# 444 "gpcb-menu.res"
char *s = N_("ChangeDrill +5 mil");
# 444 "gpcb-menu.res"
char *s = N_("Ctrl-D");
# 444 "gpcb-menu.res"
char *s = N_("Ctrl<Key>d");
# 445 "gpcb-menu.res"
char *s = N_("ChangeDrill -5 mil");
# 445 "gpcb-menu.res"
char *s = N_("Ctrl-Shift-D");
# 445 "gpcb-menu.res"
char *s = N_("Ctrl Shift<Key>d");
# 446 "gpcb-menu.res"
char *s = N_("Text Tool scale +10 mil");
# 446 "gpcb-menu.res"
char *s = N_("T");
# 446 "gpcb-menu.res"
char *s = N_("<Key>t");
# 447 "gpcb-menu.res"
char *s = N_("Text Tool scale -10 mil");
# 447 "gpcb-menu.res"
char *s = N_("Shift-T");
# 447 "gpcb-menu.res"
char *s = N_("Shift<Key>t");
# 448 "gpcb-menu.res"
char *s = N_("Via Tool size +5 mil");
# 448 "gpcb-menu.res"
char *s = N_("Shift-V");
# 448 "gpcb-menu.res"
char *s = N_("Shift<Key>v");
# 449 "gpcb-menu.res"
char *s = N_("Via Tool size -5 mil");
# 449 "gpcb-menu.res"
char *s = N_("Shift-Ctrl-V");
# 449 "gpcb-menu.res"
char *s = N_("Shift Ctrl<Key>v");
# 450 "gpcb-menu.res"
char *s = N_("Via Tool drill +5 mil");
# 450 "gpcb-menu.res"
char *s = N_("Ctrl-L");
# 450 "gpcb-menu.res"
char *s = N_("Ctrl<Key>l");
# 451 "gpcb-menu.res"
char *s = N_("Via Tool drill -5 mil");
# 451 "gpcb-menu.res"
char *s = N_("Ctrl-Shift-L");
# 451 "gpcb-menu.res"
char *s = N_("Ctrl Shift<Key>l");
# 452 "gpcb-menu.res"
char *s = N_("AddRats Selected");
# 452 "gpcb-menu.res"
char *s = N_("Shift-W");
# 452 "gpcb-menu.res"
char *s = N_("Shift<Key>w");
# 453 "gpcb-menu.res"
char *s = N_("Add All Rats");
# 453 "gpcb-menu.res"
char *s = N_("W");
# 453 "gpcb-menu.res"
char *s = N_("<Key>w");
# 454 "gpcb-menu.res"
char *s = N_("Cycle Clip");
# 454 "gpcb-menu.res"
char *s = N_("/");
# 454 "gpcb-menu.res"
char *s = N_("<Key>/");
# 455 "gpcb-menu.res"
char *s = N_("Arrow Mode");
# 455 "gpcb-menu.res"
char *s = N_("Space");
# 455 "gpcb-menu.res"
char *s = N_("<Key>space");
# 456 "gpcb-menu.res"
char *s = N_("Temp Arrow ON");
# 456 "gpcb-menu.res"
char *s = N_("[");
# 456 "gpcb-menu.res"
char *s = N_("<Key>[");
# 457 "gpcb-menu.res"
char *s = N_("Temp Arrow OFF");
# 457 "gpcb-menu.res"
char *s = N_("]");
# 457 "gpcb-menu.res"
char *s = N_("<Key>]");
# 459 "gpcb-menu.res"
char *s = N_("Step Up");
# 463 "gpcb-menu.res"
char *s = N_("Up");
# 459 "gpcb-menu.res"
char *s = N_("<Key>Up");
# 460 "gpcb-menu.res"
char *s = N_("Step Down");
# 464 "gpcb-menu.res"
char *s = N_("Down");
# 460 "gpcb-menu.res"
char *s = N_("<Key>Down");
# 461 "gpcb-menu.res"
char *s = N_("Step Left");
# 465 "gpcb-menu.res"
char *s = N_("Left");
# 461 "gpcb-menu.res"
char *s = N_("<Key>Left");
# 462 "gpcb-menu.res"
char *s = N_("Step Right");
# 466 "gpcb-menu.res"
char *s = N_("Right");
# 462 "gpcb-menu.res"
char *s = N_("<Key>Right");
# 463 "gpcb-menu.res"
char *s = N_("Step +Up");
# 463 "gpcb-menu.res"
char *s = N_("Shift<Key>Up");
# 464 "gpcb-menu.res"
char *s = N_("Step +Down");
# 464 "gpcb-menu.res"
char *s = N_("Shift<Key>Down");
# 465 "gpcb-menu.res"
char *s = N_("Step +Left");
# 465 "gpcb-menu.res"
char *s = N_("Shift<Key>Left");
# 466 "gpcb-menu.res"
char *s = N_("Step +Right");
# 466 "gpcb-menu.res"
char *s = N_("Shift<Key>Right");
# 467 "gpcb-menu.res"
char *s = N_("Click");
# 467 "gpcb-menu.res"
char *s = N_("Enter");
# 467 "gpcb-menu.res"
char *s = N_("<Key>Enter");
# 475 "gpcb-menu.res"
char *s = N_("Window");
# 476 "gpcb-menu.res"
char *s = N_("Library");
# 476 "gpcb-menu.res"
char *s = N_("i");
# 476 "gpcb-menu.res"
char *s = N_("<Key>i");
# 477 "gpcb-menu.res"
char *s = N_("Message Log");
# 478 "gpcb-menu.res"
char *s = N_("DRC Check");
# 479 "gpcb-menu.res"
char *s = N_("Netlist");
# 480 "gpcb-menu.res"
char *s = N_("Command Entry");
# 480 "gpcb-menu.res"
char *s = N_(":");
# 480 "gpcb-menu.res"
char *s = N_("<Key>:");
# 481 "gpcb-menu.res"
char *s = N_("Pinout");
# 481 "gpcb-menu.res"
char *s = N_("Shift-D");
# 481 "gpcb-menu.res"
char *s = N_("Shift<Key>d");
# 483 "gpcb-menu.res"
char *s = N_("About...");
# 491 "gpcb-menu.res"
char *s = N_("Operations on selections");
# 492 "gpcb-menu.res"
char *s = N_("Unselect all objects");
# 494 "gpcb-menu.res"
char *s = N_("Copy selection to buffer");
# 500 "gpcb-menu.res"
char *s = N_("Cut selection to buffer");
# 508 "gpcb-menu.res"
char *s = N_("Auto place selected elements");
# 509 "gpcb-menu.res"
char *s = N_("Autoroute selected elements");
# 512 "gpcb-menu.res"
char *s = N_("Operations on this location");
# 516 "gpcb-menu.res"
char *s = N_("Undo last operation");
# 517 "gpcb-menu.res"
char *s = N_("Redo last undone operation");
# 520 "gpcb-menu.res"
char *s = N_("None");
# 521 "gpcb-menu.res"
char *s = N_("Via");
# 521 "gpcb-menu.res"
char *s = N_("F1");
# 521 "gpcb-menu.res"
char *s = N_("<Key>F1");
# 522 "gpcb-menu.res"
char *s = N_("Line");
# 522 "gpcb-menu.res"
char *s = N_("F2");
# 522 "gpcb-menu.res"
char *s = N_("<Key>F2");
# 523 "gpcb-menu.res"
char *s = N_("Arc");
# 523 "gpcb-menu.res"
char *s = N_("F3");
# 523 "gpcb-menu.res"
char *s = N_("<Key>F3");
# 524 "gpcb-menu.res"
char *s = N_("F4");
# 524 "gpcb-menu.res"
char *s = N_("<Key>F4");
# 525 "gpcb-menu.res"
char *s = N_("Rectangle");
# 525 "gpcb-menu.res"
char *s = N_("F5");
# 525 "gpcb-menu.res"
char *s = N_("<Key>F5");
# 526 "gpcb-menu.res"
char *s = N_("Polygon");
# 526 "gpcb-menu.res"
char *s = N_("F6");
# 526 "gpcb-menu.res"
char *s = N_("<Key>F6");
# 527 "gpcb-menu.res"
char *s = N_("Polygon Hole");
# 528 "gpcb-menu.res"
char *s = N_("F7");
# 528 "gpcb-menu.res"
char *s = N_("<Key>F7");
# 529 "gpcb-menu.res"
char *s = N_("F8");
# 529 "gpcb-menu.res"
char *s = N_("<Key>F8");
# 530 "gpcb-menu.res"
char *s = N_("Rotate");
# 530 "gpcb-menu.res"
char *s = N_("F9");
# 530 "gpcb-menu.res"
char *s = N_("<Key>F9");
# 531 "gpcb-menu.res"
char *s = N_("Thermal");
# 531 "gpcb-menu.res"
char *s = N_("F10");
# 531 "gpcb-menu.res"
char *s = N_("<Key>F10");
# 532 "gpcb-menu.res"
char *s = N_("Arrow");
# 532 "gpcb-menu.res"
char *s = N_("F11");
# 532 "gpcb-menu.res"
char *s = N_("<Key>F11");
# 533 "gpcb-menu.res"
char *s = N_("Insert Point");
# 533 "gpcb-menu.res"
char *s = N_("Insert");
# 533 "gpcb-menu.res"
char *s = N_("<Key>Insert");
# 534 "gpcb-menu.res"
char *s = N_("Move");
# 535 "gpcb-menu.res"
char *s = N_("Copy");
# 536 "gpcb-menu.res"
char *s = N_("Lock");
# 536 "gpcb-menu.res"
char *s = N_("F12");
# 536 "gpcb-menu.res"
char *s = N_("<Key>F12");
# 537 "gpcb-menu.res"
char *s = N_("Cancel");
# 537 "gpcb-menu.res"
char *s = N_("Esc");
# 537 "gpcb-menu.res"
char *s = N_("<Key>Escape");
