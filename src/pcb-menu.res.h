# 32 "pcb-menu.res"
char *s = N_("About...");
# 33 "pcb-menu.res"
char *s = N_("Save layout");
# 33 "pcb-menu.res"
char *s = N_("Ctrl-S");
# 33 "pcb-menu.res"
char *s = N_("Ctrl<Key>s");
# 34 "pcb-menu.res"
char *s = N_("Save layout as...");
# 34 "pcb-menu.res"
char *s = N_("Shift Ctrl-S");
# 34 "pcb-menu.res"
char *s = N_("Shift Ctrl<Key>s");
# 35 "pcb-menu.res"
char *s = N_("Revert");
# 37 "pcb-menu.res"
char *s = N_("Import Schematics");
# 38 "pcb-menu.res"
char *s = N_("gschem");
# 39 "pcb-menu.res"
char *s = N_("TinyCAD");
# 41 "pcb-menu.res"
char *s = N_("Load layout");
# 42 "pcb-menu.res"
char *s = N_("Load element data to paste-buffer");
# 43 "pcb-menu.res"
char *s = N_("Load layout data to paste-buffer");
# 44 "pcb-menu.res"
char *s = N_("Load netlist file");
# 45 "pcb-menu.res"
char *s = N_("Load vendor resource file");
# 46 "pcb-menu.res"
char *s = N_("Print layout...");
# 47 "pcb-menu.res"
char *s = N_("Export layout...");
# 48 "pcb-menu.res"
char *s = N_("Calibrate Printer...");
# 50 "pcb-menu.res"
char *s = N_("Save connection data of...");
# 51 "pcb-menu.res"
char *s = N_(" a single element");
# 52 "pcb-menu.res"
char *s = N_(" all elements");
# 53 "pcb-menu.res"
char *s = N_(" unused pins");
# 55 "pcb-menu.res"
char *s = N_("Start new layout");
# 55 "pcb-menu.res"
char *s = N_("Ctrl-N");
# 55 "pcb-menu.res"
char *s = N_("Ctrl<Key>n");
# 57 "pcb-menu.res"
char *s = N_("Quit Program");
# 57 "pcb-menu.res"
char *s = N_("Ctrl-Q");
# 57 "pcb-menu.res"
char *s = N_("Ctrl<Key>q");
# 60 "pcb-menu.res"
char *s = N_("Flip up/down");
# 60 "pcb-menu.res"
char *s = N_("Tab");
# 60 "pcb-menu.res"
char *s = N_("<Key>Tab");
# 61 "pcb-menu.res"
char *s = N_("Flip left/right");
# 61 "pcb-menu.res"
char *s = N_("Shift-Tab");
# 61 "pcb-menu.res"
char *s = N_("Shift<Key>Tab");
# 62 "pcb-menu.res"
char *s = N_("Spin 180�");
# 62 "pcb-menu.res"
char *s = N_("Ctrl-Tab");
# 62 "pcb-menu.res"
char *s = N_("Ctrl<Key>Tab");
# 63 "pcb-menu.res"
char *s = N_("Swap Sides");
# 63 "pcb-menu.res"
char *s = N_("Ctrl-Shift-Tab");
# 63 "pcb-menu.res"
char *s = N_("Ctrl Shift<Key>Tab");
# 64 "pcb-menu.res"
char *s = N_("Center cursor");
# 64 "pcb-menu.res"
char *s = N_("C");
# 64 "pcb-menu.res"
char *s = N_("<Key>c");
# 65 "pcb-menu.res"
char *s = N_("Show soldermask");
# 67 "pcb-menu.res"
char *s = N_("Displayed element-name...");
# 68 "pcb-menu.res"
char *s = N_("Description");
# 69 "pcb-menu.res"
char *s = N_("Reference Designator");
# 70 "pcb-menu.res"
char *s = N_("Value");
# 71 "pcb-menu.res"
char *s = N_("Lock Names");
# 72 "pcb-menu.res"
char *s = N_("Only Names");
# 73 "pcb-menu.res"
char *s = N_("Hide Names");
# 218 "pcb-menu.res"
char *s = N_("Pinout shows number");
# 76 "pcb-menu.res"
char *s = N_("Open pinout menu");
# 451 "pcb-menu.res"
char *s = N_("Shift-D");
# 451 "pcb-menu.res"
char *s = N_("Shift<Key>d");
# 79 "pcb-menu.res"
char *s = N_("Zoom In 2X");
# 82 "pcb-menu.res"
/* xgettext:no-c-format */
char *s = N_("Zoom In 20%");
# 83 "pcb-menu.res"
char *s = N_("Z");
# 83 "pcb-menu.res"
char *s = N_("<Key>z");
# 83 "pcb-menu.res"
/* xgettext:no-c-format */
char *s = N_("Zoom Out 20%");
# 84 "pcb-menu.res"
char *s = N_("Shift-Z");
# 84 "pcb-menu.res"
char *s = N_("Shift<Key>z");
# 85 "pcb-menu.res"
char *s = N_("Zoom Out 2X");
# 86 "pcb-menu.res"
char *s = N_("Zoom Max");
# 86 "pcb-menu.res"
char *s = N_("V");
# 86 "pcb-menu.res"
char *s = N_("<Key>v");
# 87 "pcb-menu.res"
char *s = N_("Zoom Toggle");
# 87 "pcb-menu.res"
char *s = N_("`");
# 87 "pcb-menu.res"
char *s = N_("<Key>`");
# 89 "pcb-menu.res"
char *s = N_("Zoom to 0.1mil/px");
# 90 "pcb-menu.res"
char *s = N_("Zoom to 0.01mm/px");
# 91 "pcb-menu.res"
char *s = N_("Zoom to 1mil/px");
# 92 "pcb-menu.res"
char *s = N_("Zoom to 0.05mm/px");
# 93 "pcb-menu.res"
char *s = N_("Zoom to 2.5mil/px");
# 94 "pcb-menu.res"
char *s = N_("Zoom to 0.1mm/px");
# 95 "pcb-menu.res"
char *s = N_("Zoom to 10mil/px");
# 98 "pcb-menu.res"
char *s = N_("mil");
# 99 "pcb-menu.res"
char *s = N_("mm");
# 100 "pcb-menu.res"
char *s = N_("Display grid");
# 101 "pcb-menu.res"
char *s = N_("Realign grid");
# 102 "pcb-menu.res"
char *s = N_("No Grid");
# 104 "pcb-menu.res"
char *s = N_("0.1 mil");
# 105 "pcb-menu.res"
char *s = N_("1 mil");
# 106 "pcb-menu.res"
char *s = N_("5 mil");
# 107 "pcb-menu.res"
char *s = N_("10 mil");
# 108 "pcb-menu.res"
char *s = N_("25 mil");
# 109 "pcb-menu.res"
char *s = N_("50 mil");
# 110 "pcb-menu.res"
char *s = N_("100 mil");
# 112 "pcb-menu.res"
char *s = N_("0.01 mm");
# 113 "pcb-menu.res"
char *s = N_("0.05 mm");
# 114 "pcb-menu.res"
char *s = N_("0.1 mm");
# 115 "pcb-menu.res"
char *s = N_("0.25 mm");
# 116 "pcb-menu.res"
char *s = N_("0.5 mm");
# 117 "pcb-menu.res"
char *s = N_("1 mm");
# 119 "pcb-menu.res"
char *s = N_("Grid -");
# 119 "pcb-menu.res"
char *s = N_("Shift-G");
# 119 "pcb-menu.res"
char *s = N_("Shift<Key>g");
# 120 "pcb-menu.res"
char *s = N_("Grid +");
# 120 "pcb-menu.res"
char *s = N_("G");
# 120 "pcb-menu.res"
char *s = N_("<Key>g");
# 123 "pcb-menu.res"
char *s = N_("Shown Layers");
# 126 "pcb-menu.res"
char *s = N_("Edit Layer Groups");
# 128 "pcb-menu.res"
char *s = N_("Current Layer");
# 131 "pcb-menu.res"
char *s = N_("Delete current layer");
# 132 "pcb-menu.res"
char *s = N_("Add new layer");
# 133 "pcb-menu.res"
char *s = N_("Move current layer up");
# 134 "pcb-menu.res"
char *s = N_("Move current layer down");
# 138 "pcb-menu.res"
char *s = N_("Undo last operation");
# 138 "pcb-menu.res"
char *s = N_("U");
# 138 "pcb-menu.res"
char *s = N_("<Key>u");
# 139 "pcb-menu.res"
char *s = N_("Redo last undone operation");
# 139 "pcb-menu.res"
char *s = N_("Shift-R");
# 139 "pcb-menu.res"
char *s = N_("Shift<Key>r");
# 140 "pcb-menu.res"
char *s = N_("Clear undo-buffer");
# 140 "pcb-menu.res"
char *s = N_("Shift-Ctrl-U");
# 140 "pcb-menu.res"
char *s = N_("Shift Ctrl<Key>u");
# 285 "pcb-menu.res"
char *s = N_("Cut selection to buffer");
# 144 "pcb-menu.res"
char *s = N_("Ctrl-X");
# 144 "pcb-menu.res"
char *s = N_("Ctrl<Key>x");
# 283 "pcb-menu.res"
char *s = N_("Copy selection to buffer");
# 147 "pcb-menu.res"
char *s = N_("Ctrl-C");
# 147 "pcb-menu.res"
char *s = N_("Ctrl<Key>c");
# 287 "pcb-menu.res"
char *s = N_("Paste buffer to layout");
# 148 "pcb-menu.res"
char *s = N_("Ctrl-V");
# 148 "pcb-menu.res"
char *s = N_("Ctrl<Key>v");
# 150 "pcb-menu.res"
char *s = N_("Unselect all");
# 150 "pcb-menu.res"
char *s = N_("Shift-Alt-A");
# 150 "pcb-menu.res"
char *s = N_("Shift Alt<Key>a");
# 151 "pcb-menu.res"
char *s = N_("Select all visible");
# 151 "pcb-menu.res"
char *s = N_("Alt-A");
# 151 "pcb-menu.res"
char *s = N_("Alt<Key>a");
# 153 "pcb-menu.res"
char *s = N_("Edit Names...");
# 154 "pcb-menu.res"
char *s = N_(" Change text on layout");
# 154 "pcb-menu.res"
char *s = N_("N");
# 154 "pcb-menu.res"
char *s = N_("<Key>n");
# 155 "pcb-menu.res"
char *s = N_(" Edit name of layout");
# 156 "pcb-menu.res"
char *s = N_(" Edit name of active layer");
# 157 "pcb-menu.res"
char *s = N_("Edit Attributes...");
# 158 "pcb-menu.res"
char *s = N_(" Layout");
# 159 "pcb-menu.res"
char *s = N_(" CurrentLayer");
# 160 "pcb-menu.res"
char *s = N_(" Element");
# 162 "pcb-menu.res"
char *s = N_("Board Sizes");
# 163 "pcb-menu.res"
char *s = N_("Route Styles");
# 166 "pcb-menu.res"
char *s = N_("Edit...");
# 169 "pcb-menu.res"
char *s = N_("Via type");
# 170 "pcb-menu.res"
char *s = N_("Through-hole");
# 170 "pcb-menu.res"
char *s = N_("Xtrl-Shift-P");
# 215 "pcb-menu.res"
char *s = N_("Ctrl Shift<Key>p");
# 171 "pcb-menu.res"
char *s = N_("Buried from");
# 171 "pcb-menu.res"
char *s = N_("Xtrl-Shift-F");
# 171 "pcb-menu.res"
char *s = N_("Ctrl Shift<Key>f");
# 172 "pcb-menu.res"
char *s = N_("Buried to");
# 172 "pcb-menu.res"
char *s = N_("Xtrl-Shift-T");
# 172 "pcb-menu.res"
char *s = N_("Ctrl Shift<Key>t");
# 176 "pcb-menu.res"
char *s = N_("None");
# 177 "pcb-menu.res"
char *s = N_("Via");
# 177 "pcb-menu.res"
char *s = N_("F1");
# 177 "pcb-menu.res"
char *s = N_("<Key>F1");
# 178 "pcb-menu.res"
char *s = N_("Line");
# 178 "pcb-menu.res"
char *s = N_("F2");
# 178 "pcb-menu.res"
char *s = N_("<Key>F2");
# 179 "pcb-menu.res"
char *s = N_("Arc");
# 179 "pcb-menu.res"
char *s = N_("F3");
# 179 "pcb-menu.res"
char *s = N_("<Key>F3");
# 180 "pcb-menu.res"
char *s = N_("Text");
# 180 "pcb-menu.res"
char *s = N_("F4");
# 180 "pcb-menu.res"
char *s = N_("<Key>F4");
# 181 "pcb-menu.res"
char *s = N_("Rectangle");
# 181 "pcb-menu.res"
char *s = N_("F5");
# 181 "pcb-menu.res"
char *s = N_("<Key>F5");
# 182 "pcb-menu.res"
char *s = N_("Polygon");
# 182 "pcb-menu.res"
char *s = N_("F6");
# 182 "pcb-menu.res"
char *s = N_("<Key>F6");
# 183 "pcb-menu.res"
char *s = N_("Polygon Hole");
# 184 "pcb-menu.res"
char *s = N_("Buffer");
# 184 "pcb-menu.res"
char *s = N_("F7");
# 184 "pcb-menu.res"
char *s = N_("<Key>F7");
# 353 "pcb-menu.res"
char *s = N_("Remove");
# 185 "pcb-menu.res"
char *s = N_("F8");
# 185 "pcb-menu.res"
char *s = N_("<Key>F8");
# 186 "pcb-menu.res"
char *s = N_("Rotate");
# 186 "pcb-menu.res"
char *s = N_("F9");
# 186 "pcb-menu.res"
char *s = N_("<Key>F9");
# 187 "pcb-menu.res"
char *s = N_("Thermal");
# 187 "pcb-menu.res"
char *s = N_("F10");
# 187 "pcb-menu.res"
char *s = N_("<Key>F10");
# 431 "pcb-menu.res"
char *s = N_("Arrow");
# 188 "pcb-menu.res"
char *s = N_("F11");
# 188 "pcb-menu.res"
char *s = N_("<Key>F11");
# 189 "pcb-menu.res"
char *s = N_("Insert Point");
# 189 "pcb-menu.res"
char *s = N_("Insert");
# 189 "pcb-menu.res"
char *s = N_("<Key>Insert");
# 190 "pcb-menu.res"
char *s = N_("Move");
# 191 "pcb-menu.res"
char *s = N_("Copy");
# 192 "pcb-menu.res"
char *s = N_("Lock");
# 192 "pcb-menu.res"
char *s = N_("F12");
# 192 "pcb-menu.res"
char *s = N_("<Key>F12");
# 193 "pcb-menu.res"
char *s = N_("Cancel");
# 193 "pcb-menu.res"
char *s = N_("Esc");
# 193 "pcb-menu.res"
char *s = N_("<Key>Escape");
# 195 "pcb-menu.res"
char *s = N_("Command");
# 195 "pcb-menu.res"
char *s = N_(":");
# 195 "pcb-menu.res"
char *s = N_("<Key>:");
# 198 "pcb-menu.res"
char *s = N_("Layer groups");
# 199 "pcb-menu.res"
char *s = N_("Edit layer groupings");
# 201 "pcb-menu.res"
char *s = N_("'All-direction' lines");
# 201 "pcb-menu.res"
char *s = N_(".");
# 201 "pcb-menu.res"
char *s = N_("<Key>.");
# 202 "pcb-menu.res"
char *s = N_("Auto swap line start angle");
# 203 "pcb-menu.res"
char *s = N_("Orthogonal moves");
# 204 "pcb-menu.res"
char *s = N_("Crosshair snaps to pins and pads");
# 205 "pcb-menu.res"
char *s = N_("Crosshair shows DRC clearance");
# 206 "pcb-menu.res"
char *s = N_("Auto enforce DRC clearance");
# 208 "pcb-menu.res"
char *s = N_("Rubber band mode");
# 209 "pcb-menu.res"
char *s = N_("Require unique element names");
# 210 "pcb-menu.res"
char *s = N_("Auto-zero delta measurements");
# 211 "pcb-menu.res"
char *s = N_("New lines, arcs clear polygons");
# 212 "pcb-menu.res"
char *s = N_("New polygons are full ones");
# 213 "pcb-menu.res"
char *s = N_("Show autorouter trials");
# 214 "pcb-menu.res"
char *s = N_("Thin draw");
# 214 "pcb-menu.res"
char *s = N_("|");
# 214 "pcb-menu.res"
char *s = N_("<Key>|");
# 215 "pcb-menu.res"
char *s = N_("Thin draw poly");
# 215 "pcb-menu.res"
char *s = N_("Ctrl-Shift-P");
# 216 "pcb-menu.res"
char *s = N_("Check polygons");
# 219 "pcb-menu.res"
char *s = N_("Pins/Via show Name/Number");
# 219 "pcb-menu.res"
char *s = N_("D");
# 219 "pcb-menu.res"
char *s = N_("<Key>d");
# 220 "pcb-menu.res"
char *s = N_("Enable vendor drill mapping");
# 221 "pcb-menu.res"
char *s = N_("Import Settings");
# 222 "pcb-menu.res"
char *s = N_("New elements added at...");
# 223 "pcb-menu.res"
char *s = N_(" Center");
# 224 "pcb-menu.res"
char *s = N_(" Mark");
# 225 "pcb-menu.res"
char *s = N_(" Crosshair");
# 227 "pcb-menu.res"
char *s = N_("Set Dispersion");
# 232 "pcb-menu.res"
char *s = N_("Select all visible objects");
# 233 "pcb-menu.res"
char *s = N_("Select all found objects");
# 234 "pcb-menu.res"
char *s = N_("Select all connected objects");
# 236 "pcb-menu.res"
char *s = N_("Unselect all objects");
# 237 "pcb-menu.res"
char *s = N_("unselect all found objects");
# 238 "pcb-menu.res"
char *s = N_("unselect all connected objects");
# 240 "pcb-menu.res"
char *s = N_("Select by name");
# 241 "pcb-menu.res"
char *s = N_("All objects");
# 278 "pcb-menu.res"
char *s = N_("Elements");
# 243 "pcb-menu.res"
char *s = N_("Pads");
# 279 "pcb-menu.res"
char *s = N_("Pins");
# 245 "pcb-menu.res"
char *s = N_("Text Objects");
# 246 "pcb-menu.res"
char *s = N_("Vias");
# 248 "pcb-menu.res"
char *s = N_("Auto-place selected elements");
# 248 "pcb-menu.res"
char *s = N_("Ctrl-P");
# 248 "pcb-menu.res"
char *s = N_("Ctrl<Key>p");
# 249 "pcb-menu.res"
char *s = N_("Disperse all elements");
# 250 "pcb-menu.res"
char *s = N_("Move selected elements to other side");
# 250 "pcb-menu.res"
char *s = N_("Shift-B");
# 250 "pcb-menu.res"
char *s = N_("Shift<Key>b");
# 251 "pcb-menu.res"
char *s = N_("Move selected to current layer");
# 251 "pcb-menu.res"
char *s = N_("Shift-M");
# 251 "pcb-menu.res"
char *s = N_("Shift<Key>m");
# 252 "pcb-menu.res"
char *s = N_("Delete selected objects");
# 252 "pcb-menu.res"
char *s = N_("Delete");
# 252 "pcb-menu.res"
char *s = N_("<Key>Delete");
# 253 "pcb-menu.res"
char *s = N_("Convert selection to element");
# 255 "pcb-menu.res"
char *s = N_("Optimize selected rats");
# 321 "pcb-menu.res"
char *s = N_("Auto-route selected rats");
# 256 "pcb-menu.res"
char *s = N_("Alt-R");
# 256 "pcb-menu.res"
char *s = N_("Alt<Key>r");
# 257 "pcb-menu.res"
char *s = N_("Rip up selected auto-routed tracks");
# 259 "pcb-menu.res"
char *s = N_("Change size of selected objects");
# 260 "pcb-menu.res"
char *s = N_("Lines -10 mil");
# 261 "pcb-menu.res"
char *s = N_("Lines +10 mil");
# 262 "pcb-menu.res"
char *s = N_("Pads -10 mil");
# 263 "pcb-menu.res"
char *s = N_("Pads +10 mil");
# 274 "pcb-menu.res"
char *s = N_("Pins -10 mil");
# 275 "pcb-menu.res"
char *s = N_("Pins +10 mil");
# 266 "pcb-menu.res"
char *s = N_("Texts -10 mil");
# 267 "pcb-menu.res"
char *s = N_("Texts +10 mil");
# 272 "pcb-menu.res"
char *s = N_("Vias -10 mil");
# 273 "pcb-menu.res"
char *s = N_("Vias +10 mil");
# 271 "pcb-menu.res"
char *s = N_("Change drilling hole of selected objects");
# 277 "pcb-menu.res"
char *s = N_("Change square-flag of selected objects");
# 289 "pcb-menu.res"
char *s = N_("Rotate buffer 90 deg CCW");
# 290 "pcb-menu.res"
char *s = N_("Shift-F7");
# 290 "pcb-menu.res"
char *s = N_("Shift<Key>F7");
# 291 "pcb-menu.res"
char *s = N_("Rotate buffer 90 deg CW");
# 292 "pcb-menu.res"
char *s = N_("Arbitrarily Rotate Buffer");
# 293 "pcb-menu.res"
char *s = N_("Mirror buffer (up/down)");
# 294 "pcb-menu.res"
char *s = N_("Mirror buffer (left/right)");
# 297 "pcb-menu.res"
char *s = N_("Clear buffer");
# 298 "pcb-menu.res"
char *s = N_("Convert buffer to element");
# 299 "pcb-menu.res"
char *s = N_("Break buffer elements to pieces");
# 300 "pcb-menu.res"
char *s = N_("Save buffer elements to file");
# 302 "pcb-menu.res"
char *s = N_("Select current buffer");
# 303 "pcb-menu.res"
char *s = N_("#1");
# 303 "pcb-menu.res"
char *s = N_("Shift-1");
# 303 "pcb-menu.res"
char *s = N_("Shift<Key>1");
# 304 "pcb-menu.res"
char *s = N_("#2");
# 304 "pcb-menu.res"
char *s = N_("Shift-2");
# 304 "pcb-menu.res"
char *s = N_("Shift<Key>2");
# 305 "pcb-menu.res"
char *s = N_("#3");
# 305 "pcb-menu.res"
char *s = N_("Shift-3");
# 305 "pcb-menu.res"
char *s = N_("Shift<Key>3");
# 306 "pcb-menu.res"
char *s = N_("#4");
# 306 "pcb-menu.res"
char *s = N_("Shift-4");
# 306 "pcb-menu.res"
char *s = N_("Shift<Key>4");
# 307 "pcb-menu.res"
char *s = N_("#5");
# 307 "pcb-menu.res"
char *s = N_("Shift-5");
# 307 "pcb-menu.res"
char *s = N_("Shift<Key>5");
# 311 "pcb-menu.res"
char *s = N_("Lookup connection to object");
# 311 "pcb-menu.res"
char *s = N_("Ctrl-F");
# 311 "pcb-menu.res"
char *s = N_("Ctrl<Key>f");
# 312 "pcb-menu.res"
char *s = N_("Reset scanned pads/pins/vias");
# 313 "pcb-menu.res"
char *s = N_("Reset scanned lines/polygons");
# 314 "pcb-menu.res"
char *s = N_("Reset all connections");
# 314 "pcb-menu.res"
char *s = N_("Shift-F");
# 314 "pcb-menu.res"
char *s = N_("Shift<Key>f");
# 316 "pcb-menu.res"
char *s = N_("Optimize rats-nest");
# 317 "pcb-menu.res"
char *s = N_("O");
# 317 "pcb-menu.res"
char *s = N_("<Key>o");
# 318 "pcb-menu.res"
char *s = N_("Erase rats-nest");
# 318 "pcb-menu.res"
char *s = N_("E");
# 318 "pcb-menu.res"
char *s = N_("<Key>e");
# 319 "pcb-menu.res"
char *s = N_("Erase selected rats");
# 319 "pcb-menu.res"
char *s = N_("Shift-E");
# 319 "pcb-menu.res"
char *s = N_("Shift<Key>e");
# 322 "pcb-menu.res"
char *s = N_("Auto-route all rats");
# 323 "pcb-menu.res"
char *s = N_("Toporouter");
# 324 "pcb-menu.res"
char *s = N_("Rip up all auto-routed tracks");
# 326 "pcb-menu.res"
char *s = N_("Auto-Optimize");
# 326 "pcb-menu.res"
char *s = N_("Shift-=");
# 326 "pcb-menu.res"
char *s = N_("Shift<Key>=");
# 327 "pcb-menu.res"
char *s = N_("Debumpify");
# 328 "pcb-menu.res"
char *s = N_("Unjaggy");
# 329 "pcb-menu.res"
char *s = N_("Vianudge");
# 330 "pcb-menu.res"
char *s = N_("Viatrim");
# 331 "pcb-menu.res"
char *s = N_("Orthopull");
# 332 "pcb-menu.res"
char *s = N_("SimpleOpts");
# 332 "pcb-menu.res"
char *s = N_("=");
# 332 "pcb-menu.res"
char *s = N_("<Key>=");
# 333 "pcb-menu.res"
char *s = N_("Miter");
# 334 "pcb-menu.res"
char *s = N_("Puller");
# 334 "pcb-menu.res"
char *s = N_("Y");
# 334 "pcb-menu.res"
char *s = N_("<Key>y");
# 335 "pcb-menu.res"
char *s = N_("Global Puller");
# 336 "pcb-menu.res"
char *s = N_("Selected");
# 337 "pcb-menu.res"
char *s = N_("Found");
# 338 "pcb-menu.res"
char *s = N_("All");
# 340 "pcb-menu.res"
char *s = N_("Only autorouted nets");
# 342 "pcb-menu.res"
char *s = N_("Design Rule Checker");
# 344 "pcb-menu.res"
char *s = N_("Apply vendor drill mapping");
# 348 "pcb-menu.res"
char *s = N_("Generate object report");
# 348 "pcb-menu.res"
char *s = N_("Ctrl-R");
# 348 "pcb-menu.res"
char *s = N_("Ctrl<Key>r");
# 349 "pcb-menu.res"
char *s = N_("Generate drill summary");
# 350 "pcb-menu.res"
char *s = N_("Report found pins/pads");
# 351 "pcb-menu.res"
char *s = N_("Report net length");
# 351 "pcb-menu.res"
char *s = N_("R");
# 351 "pcb-menu.res"
char *s = N_("<Key>r");
# 352 "pcb-menu.res"
char *s = N_("Key Bindings");
# 353 "pcb-menu.res"
char *s = N_("Backspace");
# 353 "pcb-menu.res"
char *s = N_("<Key>BackSpace");
# 373 "pcb-menu.res"
char *s = N_("Remove Connected");
# 356 "pcb-menu.res"
char *s = N_("Shift-Backspace");
# 356 "pcb-menu.res"
char *s = N_("Shift<Key>BackSpace");
# 373 "pcb-menu.res"
char *s = N_("Shift-Delete");
# 373 "pcb-menu.res"
char *s = N_("Shift<Key>Delete");
# 390 "pcb-menu.res"
char *s = N_("Set Same");
# 390 "pcb-menu.res"
char *s = N_("A");
# 390 "pcb-menu.res"
char *s = N_("<Key>a");
# 391 "pcb-menu.res"
char *s = N_("Flip Object");
# 391 "pcb-menu.res"
char *s = N_("B");
# 391 "pcb-menu.res"
char *s = N_("<Key>b");
# 392 "pcb-menu.res"
char *s = N_("Find Connections");
# 392 "pcb-menu.res"
char *s = N_("F");
# 392 "pcb-menu.res"
char *s = N_("<Key>f");
# 393 "pcb-menu.res"
char *s = N_("ToggleHideName Object");
# 393 "pcb-menu.res"
char *s = N_("H");
# 393 "pcb-menu.res"
char *s = N_("<Key>h");
# 394 "pcb-menu.res"
char *s = N_("ToggleHideName SelectedElement");
# 394 "pcb-menu.res"
char *s = N_("Shift-H");
# 394 "pcb-menu.res"
char *s = N_("Shift<Key>h");
# 395 "pcb-menu.res"
char *s = N_("ChangeHole Object");
# 395 "pcb-menu.res"
char *s = N_("Ctrl-H");
# 395 "pcb-menu.res"
char *s = N_("Ctrl<Key>h");
# 396 "pcb-menu.res"
char *s = N_("ChangeJoin Object");
# 396 "pcb-menu.res"
char *s = N_("J");
# 396 "pcb-menu.res"
char *s = N_("<Key>j");
# 397 "pcb-menu.res"
char *s = N_("ChangeJoin SelectedObject");
# 397 "pcb-menu.res"
char *s = N_("Shift-J");
# 397 "pcb-menu.res"
char *s = N_("Shift<Key>j");
# 398 "pcb-menu.res"
char *s = N_("Clear Object +");
# 398 "pcb-menu.res"
char *s = N_("K");
# 398 "pcb-menu.res"
char *s = N_("<Key>k");
# 399 "pcb-menu.res"
char *s = N_("Clear Object -");
# 399 "pcb-menu.res"
char *s = N_("Shift-K");
# 399 "pcb-menu.res"
char *s = N_("Shift<Key>k");
# 400 "pcb-menu.res"
char *s = N_("Clear Selected +");
# 400 "pcb-menu.res"
char *s = N_("Ctrl-K");
# 400 "pcb-menu.res"
char *s = N_("Ctrl<Key>k");
# 401 "pcb-menu.res"
char *s = N_("Clear Selected -");
# 401 "pcb-menu.res"
char *s = N_("Shift-Ctrl-K");
# 401 "pcb-menu.res"
char *s = N_("Shift Ctrl<Key>k");
# 402 "pcb-menu.res"
char *s = N_("Line Tool size +");
# 402 "pcb-menu.res"
char *s = N_("L");
# 402 "pcb-menu.res"
char *s = N_("<Key>l");
# 403 "pcb-menu.res"
char *s = N_("Line Tool size -");
# 403 "pcb-menu.res"
char *s = N_("Shift-L");
# 403 "pcb-menu.res"
char *s = N_("Shift<Key>l");
# 404 "pcb-menu.res"
char *s = N_("Move Object to current layer");
# 404 "pcb-menu.res"
char *s = N_("M");
# 404 "pcb-menu.res"
char *s = N_("<Key>m");
# 405 "pcb-menu.res"
char *s = N_("MarkCrosshair");
# 405 "pcb-menu.res"
char *s = N_("Ctrl-M");
# 405 "pcb-menu.res"
char *s = N_("Ctrl<Key>m");
# 406 "pcb-menu.res"
char *s = N_("Select shortest rat");
# 406 "pcb-menu.res"
char *s = N_("Shift-N");
# 406 "pcb-menu.res"
char *s = N_("Shift<Key>n");
# 407 "pcb-menu.res"
char *s = N_("AddRats to selected pins");
# 407 "pcb-menu.res"
char *s = N_("Shift-O");
# 407 "pcb-menu.res"
char *s = N_("Shift<Key>o");
# 413 "pcb-menu.res"
char *s = N_("ChangeOctagon Object");
# 413 "pcb-menu.res"
char *s = N_("Ctrl-O");
# 413 "pcb-menu.res"
char *s = N_("Ctrl<Key>o");
# 414 "pcb-menu.res"
char *s = N_("Polygon PreviousPoint");
# 414 "pcb-menu.res"
char *s = N_("P");
# 414 "pcb-menu.res"
char *s = N_("<Key>p");
# 415 "pcb-menu.res"
char *s = N_("Polygon Close");
# 415 "pcb-menu.res"
char *s = N_("Shift-P");
# 415 "pcb-menu.res"
char *s = N_("Shift<Key>p");
# 416 "pcb-menu.res"
char *s = N_("ChangeSquare Object");
# 416 "pcb-menu.res"
char *s = N_("Q");
# 416 "pcb-menu.res"
char *s = N_("<Key>q");
# 417 "pcb-menu.res"
char *s = N_("ChangeSize +");
# 417 "pcb-menu.res"
char *s = N_("S");
# 417 "pcb-menu.res"
char *s = N_("<Key>s");
# 418 "pcb-menu.res"
char *s = N_("ChangeSize -");
# 418 "pcb-menu.res"
char *s = N_("Shift-S");
# 418 "pcb-menu.res"
char *s = N_("Shift<Key>s");
# 419 "pcb-menu.res"
char *s = N_("ChangeDrill +5 mil");
# 419 "pcb-menu.res"
char *s = N_("Alt-S");
# 419 "pcb-menu.res"
char *s = N_("Alt<Key>s");
# 420 "pcb-menu.res"
char *s = N_("ChangeDrill -5 mil");
# 420 "pcb-menu.res"
char *s = N_("Alt-Shift-S");
# 420 "pcb-menu.res"
char *s = N_("Alt Shift<Key>s");
# 421 "pcb-menu.res"
char *s = N_("Text Tool scale +10 mil");
# 421 "pcb-menu.res"
char *s = N_("T");
# 421 "pcb-menu.res"
char *s = N_("<Key>t");
# 422 "pcb-menu.res"
char *s = N_("Text Tool scale -10 mil");
# 422 "pcb-menu.res"
char *s = N_("Shift-T");
# 422 "pcb-menu.res"
char *s = N_("Shift<Key>t");
# 423 "pcb-menu.res"
char *s = N_("Via Tool size +5 mil");
# 423 "pcb-menu.res"
char *s = N_("Shift-V");
# 423 "pcb-menu.res"
char *s = N_("Shift<Key>v");
# 424 "pcb-menu.res"
char *s = N_("Via Tool size -5 mil");
# 424 "pcb-menu.res"
char *s = N_("Shift-Ctrl-V");
# 424 "pcb-menu.res"
char *s = N_("Shift Ctrl<Key>v");
# 425 "pcb-menu.res"
char *s = N_("Via Tool drill +5 mil");
# 425 "pcb-menu.res"
char *s = N_("Alt-V");
# 425 "pcb-menu.res"
char *s = N_("Alt<Key>v");
# 426 "pcb-menu.res"
char *s = N_("Via Tool drill -5 mil");
# 426 "pcb-menu.res"
char *s = N_("Alt-Shift-V");
# 426 "pcb-menu.res"
char *s = N_("Alt Shift<Key>v");
# 427 "pcb-menu.res"
char *s = N_("AddRats Selected");
# 427 "pcb-menu.res"
char *s = N_("Shift-W");
# 427 "pcb-menu.res"
char *s = N_("Shift<Key>w");
# 428 "pcb-menu.res"
char *s = N_("Add All Rats");
# 428 "pcb-menu.res"
char *s = N_("W");
# 428 "pcb-menu.res"
char *s = N_("<Key>w");
# 429 "pcb-menu.res"
char *s = N_("Undo");
# 429 "pcb-menu.res"
char *s = N_("Alt-Z");
# 429 "pcb-menu.res"
char *s = N_("Alt<Key>z");
# 430 "pcb-menu.res"
char *s = N_("Cycle Clip");
# 430 "pcb-menu.res"
char *s = N_("/");
# 430 "pcb-menu.res"
char *s = N_("<Key>/");
# 431 "pcb-menu.res"
char *s = N_("Space");
# 431 "pcb-menu.res"
char *s = N_("<Key>space");
# 432 "pcb-menu.res"
char *s = N_("Temp Arrow ON");
# 432 "pcb-menu.res"
char *s = N_("[");
# 432 "pcb-menu.res"
char *s = N_("<Key>[");
# 433 "pcb-menu.res"
char *s = N_("Temp Arrow OFF");
# 433 "pcb-menu.res"
char *s = N_("]");
# 433 "pcb-menu.res"
char *s = N_("<Key>]");
# 435 "pcb-menu.res"
char *s = N_("Step Up");
# 439 "pcb-menu.res"
char *s = N_("Up");
# 435 "pcb-menu.res"
char *s = N_("<Key>Up");
# 436 "pcb-menu.res"
char *s = N_("Step Down");
# 440 "pcb-menu.res"
char *s = N_("Down");
# 436 "pcb-menu.res"
char *s = N_("<Key>Down");
# 437 "pcb-menu.res"
char *s = N_("Step Left");
# 441 "pcb-menu.res"
char *s = N_("Left");
# 437 "pcb-menu.res"
char *s = N_("<Key>Left");
# 438 "pcb-menu.res"
char *s = N_("Step Right");
# 442 "pcb-menu.res"
char *s = N_("Right");
# 438 "pcb-menu.res"
char *s = N_("<Key>Right");
# 439 "pcb-menu.res"
char *s = N_("Step +Up");
# 439 "pcb-menu.res"
char *s = N_("Shift<Key>Up");
# 440 "pcb-menu.res"
char *s = N_("Step +Down");
# 440 "pcb-menu.res"
char *s = N_("Shift<Key>Down");
# 441 "pcb-menu.res"
char *s = N_("Step +Left");
# 441 "pcb-menu.res"
char *s = N_("Shift<Key>Left");
# 442 "pcb-menu.res"
char *s = N_("Step +Right");
# 442 "pcb-menu.res"
char *s = N_("Shift<Key>Right");
# 443 "pcb-menu.res"
char *s = N_("Click");
# 443 "pcb-menu.res"
char *s = N_("Enter");
# 443 "pcb-menu.res"
char *s = N_("<Key>Enter");
# 447 "pcb-menu.res"
char *s = N_("Board Layout");
# 448 "pcb-menu.res"
char *s = N_("Library");
# 449 "pcb-menu.res"
char *s = N_("Message Log");
# 450 "pcb-menu.res"
char *s = N_("Netlist");
# 451 "pcb-menu.res"
char *s = N_("Pinout");
