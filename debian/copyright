Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0
Upstream-Name: pcb
Upstream-Contact: harry eaton <haceaton@aplcomm.jhuapl.edu>
Source: http://pcb.gpleda.org


Files: *
Copyright: 1994-1997,2004,2010 Thomas Nau <Thomas.Nau@rz.uni-ulm.de>
           1997-2007,2009 harry eaton <haceaton@aplcomm.jhuapl.edu>
           2001 C. Scott Ananian
           2003-2008 DJ Delorie <djdelorie@users.sourceforge.net>
           2010-2019 PCB Contributors (See ChangeLog for details)
           2009 Anthony Blake <anthonix@anthonix.resnet.scms.waikato.ac.nz>
           2011 Andrew Poelstra <apoelstra@wpsoftware.net>
           2010 Alberto Maccioni <alberto.maccioni@gmail.com>
           2003-2010,2017 Dan McMahill <danmc@users.sourceforge.net>
           2018 Charles Parker <parker.charles@gmail.com>]
License: GPL-2+

Files: data/pcb.appdata.xml*
Copyright: 2010-2019 PCB Contributors (See ChangeLog for details)
License: GFDL-1.3
 Permission is granted to copy, distribute and/or modify this document under
 the terms of the GNU Free Documentation License, Version 1.3; with no 
 Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.  A copy of 
 the license is included in the section entitled ``GNU Free Documentation 
 License''.
 .
 On Debian systems, the complete text of the GNU GFDL 1.3 licenses can be found
 at `/usr/share/common-licenses/GFDL-1.3'.

Files: src/hid/bom_md/bom_md.c
Copyright: 2020 Bert Timmerman <bert.timmerman@xs4all.nl>
License: GPL-2+

Files: src/edif.*
       src/parse_y.*
       src/res_parse.*
Copyright: 1984, 1989-1990, 2000-2015, 2018-2020 Free Software Foundation
License: GPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU GPL3 licenses can be found at
 `/usr/share/common-licenses/GPL-3'.

Files: debian/*
Copyright: 2003-2008 Hamish Moffatt <hamish@debian.org>
           1996 Michael Mattice <mattice@mattice.reslife.okstate.edu>
           1997-1999 Hartmut Koptein <koptein@debian.org>
           2009-2013,2017,2019,2023 أحمد المحمودي (Ahmed El-Mahmoudy) <aelmahmoudy@users.sourceforge.net>
License: GPL-2+

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 USA.
 .
 On Debian systems, the complete text of the GNU GPL2 licenses can be found at
 `/usr/share/common-licenses/GPL-2'.

